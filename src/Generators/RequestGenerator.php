<?php
namespace TeamOptimus\Modularavel\Generators;

/**
 * Class RequestGenerator
 * @package TeamOptimus\Modularavel\Generators;
 */
class RequestGenerator extends Generator
{
    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'request';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode() );
    }

    /**
     * Get generator path config node.
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'requests';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath( $this->getPathConfigNode() , true) . '/' . $this->getRequestName() . '.php';
    }

    /**
     * Get base path of destination file.
     *
     * @return string
     */
    public function getBasePath()
    {
        return config('modularavel.base_path', app()->path() );
    }


    /**
     * Gets request name based on model
     *
     * @return string
     */
    public function getRequestName()
    {

        return ucfirst( $this->getName() ) ."Request";
    }


    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'request_name' => $this->getRequestName(),
        ]);
    }

}
