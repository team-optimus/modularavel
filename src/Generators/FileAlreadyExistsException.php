<?php
namespace TeamOptimus\Modularavel\Generators;

use Exception;

/**
 * Class FileAlreadyExistsException
 * @package TeamOptimus\Modularavel\Generators;
 */
class FileAlreadyExistsException extends Exception
{
}