<?php

namespace TeamOptimus\Modularavel\Providers;

use Illuminate\Support\ServiceProvider;

class ModularServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../resources/modularavel.php' => config_path('modularavel.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\BindCommand');
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\ModularCommand');
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\ControllerCommand');
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\RequestCommand');
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\RepositoryCommand');
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\ModularRouteCommand');
        $this->commands('TeamOptimus\Modularavel\Generators\Commands\StubsCommand');
    }
}
